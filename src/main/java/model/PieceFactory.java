package model;


import static model.PieceName.*;


public class PieceFactory {

    public static Piece createPiece(PieceName pieceName, int coordinateX, int coordinateY) {

        switch (pieceName) {
            case WhitePawn:
                return new Piece(WhitePawn, coordinateX, coordinateY);
            case WhiteRock:
                return new Piece(WhiteRock, coordinateX, coordinateY);
            case WhiteKing:
                return new Piece(WhiteKing, coordinateX, coordinateY);
        }
        return null;

    }
}
//   WhitePawn,
//           WhiteRock,
//           WhiteBishop,
//           WhiteQueen,
//           WhiteKing,
//           WhiteKnight,
//           BlackPawn,
//           BlackRock,
//           BlackBishop,
//           BlackQueen,
//           BlackKing,
//           BlackKnight,