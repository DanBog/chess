package model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Piece {

    private Enum<PieceName> name;
    private int coordinateX;
    private int coordinateY;

}

