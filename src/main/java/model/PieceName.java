package model;

public enum PieceName {
    WhitePawn,
    WhiteRock,
    WhiteBishop,
    WhiteQueen,
    WhiteKing,
    WhiteKnight,
    BlackPawn, 
    BlackRock,
    BlackBishop,
    BlackQueen,
    BlackKing,
    BlackKnight,
}
