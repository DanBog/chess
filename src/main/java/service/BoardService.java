package service;

import model.PieceName;

public class BoardService {



    public static void printBoard() {

        System.out.print("   (#A)(#B)(#C)(#D)(#E)(#F)(#G)(#H)");
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (j == 0) {
                    System.out.println();
                    System.out.print("(" + (i + 1) + ")");
                }
                if (i == 0 && j == 2) {
                    System.out.print(" "+PieceService.printPiece(PieceName.WhiteBishop));
                }
            }
        }
    }
}
