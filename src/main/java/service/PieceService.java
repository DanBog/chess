package service;

import model.Piece;
import model.PieceName;

import java.util.ArrayList;
import java.util.List;

import static model.PieceName.*;

public class PieceService {


    private static final int whiteKing = 2654;
    private static final int whitePawn = 2659;
    private static final int whiteQueen = 2655;
    private static final int whiteRook = 2656;
    private static final int whiteBishop = 2657;
    private static final int whiteKnight = 2658;
    private static final int blackKing = 0x265A;
    private static final int blackPawn = 0x265F;
    private static final int blackQueen = 0x265B;
    private static final int blackRook = 0x265C;
    private static final int blackBishop = 0x265D;
    private static final int blackKnight = 0x265E;

    private static final int finishMetodMark = 2615;


    public static char printPiece(PieceName pieceName) {
        switch (pieceName) {
            case BlackKing:
                return (char) (Integer.parseInt(String.valueOf(blackKing)));
            case WhiteKing:
                return (char) (Integer.parseInt(String.valueOf(whiteKing), 16));
            case BlackPawn:
                return (char) (Integer.parseInt(String.valueOf(blackPawn)));
            case WhitePawn:
                return (char) (Integer.parseInt(String.valueOf(whitePawn), 16));
            case BlackQueen:
                return (char) (Integer.parseInt(String.valueOf(blackQueen)));
            case WhiteQueen:
                return (char) (Integer.parseInt(String.valueOf(whiteQueen), 16));
            case BlackRock:
                return (char) (Integer.parseInt(String.valueOf(blackRook)));
            case WhiteRock:
                return (char) (Integer.parseInt(String.valueOf(whiteRook), 16));
            case BlackBishop:
                 return (char) (Integer.parseInt(String.valueOf(blackBishop)));
            case WhiteBishop:
                return (char) (Integer.parseInt(String.valueOf(whiteBishop), 16));
            case BlackKnight:
                return (char) (Integer.parseInt(String.valueOf(blackKnight)));
            case WhiteKnight:
                return (char) (Integer.parseInt(String.valueOf(whiteKnight), 16));
        }
        return (char) (Integer.parseInt(String.valueOf(finishMetodMark)));

    }

    public static ArrayList<Piece> pieceStartList() {
        ArrayList<Piece> pieceStartList = new ArrayList<Piece>();
        pieceStartList.add(new Piece(BlackPawn, 1, 2));
        pieceStartList.add(new Piece(BlackPawn, 1, 3));
        pieceStartList.add(new Piece(BlackPawn, 1, 4));
        pieceStartList.add(new Piece(BlackPawn, 1, 5));
        pieceStartList.add(new Piece(BlackPawn, 1, 6));
        pieceStartList.add(new Piece(BlackPawn, 1, 7));
        pieceStartList.add(new Piece(BlackPawn, 1, 8));
        pieceStartList.add(new Piece(BlackPawn, 1, 9));
        pieceStartList.add(new Piece(BlackRock, 0, 2));
        pieceStartList.add(new Piece(BlackRock, 0, 9));
        pieceStartList.add(new Piece(BlackKnight, 0, 3));
        pieceStartList.add(new Piece(BlackKnight, 0, 8));
        pieceStartList.add(new Piece(BlackBishop, 0, 4));
        pieceStartList.add(new Piece(BlackBishop, 0, 7));
        pieceStartList.add(new Piece(BlackQueen, 0, 6));
        pieceStartList.add(new Piece(BlackKing, 0, 5));
        pieceStartList.add(new Piece(WhiteKing, 5, 5));

        return pieceStartList;
    }


    public static Piece findPiece(List<Piece> pieceList, int x, int y) {
        for (Piece p : pieceList) {
            if (p.getCoordinateX() == x && p.getCoordinateY() == y)
                return p;
        }
        return null;
    }
}
