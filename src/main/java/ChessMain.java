import model.Piece;
import model.PieceName;
import service.BoardService;
import service.PieceService;

import java.util.List;


public class ChessMain {


    public static void main(String[] args) {

        System.out.print(PieceService.printPiece(PieceName.WhiteBishop));
        System.out.println(PieceService.printPiece(PieceName.BlackBishop));
        System.out.print(PieceService.printPiece(PieceName.WhiteKnight));
        System.out.println(PieceService.printPiece(PieceName.BlackKnight));
        System.out.print(PieceService.printPiece(PieceName.WhitePawn));
        System.out.println(PieceService.printPiece(PieceName.BlackPawn));
        System.out.print(PieceService.printPiece(PieceName.WhiteRock));
        System.out.println(PieceService.printPiece(PieceName.BlackRock));
        System.out.print(PieceService.printPiece(PieceName.WhiteQueen));
        System.out.println(PieceService.printPiece(PieceName.BlackQueen));
        System.out.print(PieceService.printPiece(PieceName.WhiteKing));
        System.out.println(PieceService.printPiece(PieceName.BlackKing));

        BoardService.printBoard();
        System.out.println();

        List<Piece> pieceList = PieceService.pieceStartList();
        Piece p = PieceService.findPiece(pieceList, 0, 3);
        System.out.println(PieceService.printPiece((PieceName) p.getName()));
        Piece pp = PieceService.findPiece(pieceList,0,4);
        System.out.println(PieceService.printPiece((PieceName) pp.getName()));
        Piece ppp = PieceService.findPiece(pieceList,5,5);
        System.out.println(PieceService.printPiece((PieceName) ppp.getName()));
    }
}
